import { configureStore } from "@reduxjs/toolkit";
import counterReducer from './features/counter/couterSlice'
import { productApi } from "./service/dummyData";
import { setupListeners } from "@reduxjs/toolkit/query";
export const store = configureStore({
    reducer: {
        counter: counterReducer,
        [productApi.reducerPath]: productApi.reducer,
    },
    middleware:(getDefaultMiddleware) => 
        getDefaultMiddleware().concat(productApi.middleware)
});

setupListeners(store.dispatch)