import './App.css'
import { NavLink, Route, Routes } from 'react-router-dom'
import RTK from './components/rtk'
import Counter from './components/Counter'
import AllProducts from './components/AllProducts'
import SpecificProduct from './components/SpecificProduct'

function App() {
  return (
    <>
      <NavLink to={''}>Home</NavLink>
      <NavLink to={'/rtk'}>RTK</NavLink>
      <NavLink to={'/Counter'}>Counter</NavLink>
      <NavLink to={'/AllProducts'}>AllProducts</NavLink>
      <NavLink to={'/SpecificProduct'}>SpecificProduct</NavLink>
      <Routes>
        <Route path='/rtk' element={<RTK/>}></Route>
        <Route path='/Counter' element={<Counter/>}></Route>
        <Route path='/AllProducts' element={<AllProducts/>}></Route>
        <Route path='/SpecificProduct' element={<SpecificProduct/>}></Route>
      </Routes>
    </>
  )
}

export default App
