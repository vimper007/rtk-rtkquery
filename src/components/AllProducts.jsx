import React from 'react'
import { useGetAllProductQuery } from '../app/service/dummyData'
const AllProducts = () => {
    const { data, isLoading, isError } = useGetAllProductQuery();
    if(isError){
        return <p>Error</p>
    }
    if(isLoading){
        return <p>Loading...</p>
    }
    
    return (
        <div>
            <h2>All Products</h2>
            {data?.products?.map(p=>(
                <p key={p.id}>{p.title}</p>
            ))}
        </div>
    )
}

export default AllProducts