import React from 'react'
import { useGetProductByIdQuery } from '../app/service/dummyData'

const SpecificProduct = () => {
    const {data,isError,isLoading} = useGetProductByIdQuery(3);
  return (
    <div>
        <h1>Specific</h1>
        {isLoading &&
            <h2>Loading...</h2>
        }
        {isError &&
            <h2>Error</h2>
        }
        {data &&
        <p>{data.title}</p>
        }
    </div>
  )
}

export default SpecificProduct